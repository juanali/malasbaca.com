<?php

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//menampilkan halaman login pada saat pertama kali membuka web
		$this->load->view('v_login');
	}

	function login(){
		//mengambil data inputan user dari view
		$username = $this->input->post('email');
		$password = $this->input->post('password');
		//menggabungkan inputan menjadi array yang akan di proses di model
		$where = array(
			'email' => $username,
			'password' => $password
			);

		//proses pengecekan user di database
		$cek = $this->db->get_where('users',$where);
		$data = $cek->result();
		if(!empty($data)){
			//jika user di temukan maka akan di alihkan ke dashboard
			$data_session = array(
				'nama' => $username,
				'foto' => $data->foto,
				'status' => "login"
				);

		$this->session->set_userdata($data_session);

		redirect(base_url("admin/home"));

		}else{
			//jika tidak ditemukan maka akan ada notif
			echo "<script>
							alert('username atau password salah!');
							document.location='".base_url()."admin';
						</script>";
		}
	}

	function logout(){
		//menghapus session user di browser
		//note:session adalah data yang di simpan di browser
		$this->session->sess_destroy();
		redirect(base_url('admin'));
	}

	public function home()
	{
		//menampilkan halaman dashboard admin
		$this->load->view('layouts/master');
	}

	public function tampilArtikel()
	{
		$query = $this->db->get('article');
		$data['artikel'] = $query->result();
		$this->load->view('layouts/artikel/v_tampil',$data);
	}

	public function formArtikel()
	{
		$this->load->view('layouts/artikel/v_tambah');
	}

	public function addArtikel()
	{
		$post = $this->input->post();


		$config['upload_path'] = './assets/images';
		$config['allowed_types'] = 'gif|jpg|png';
		// load library upload
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('userfile')) {
				$error = $this->upload->display_errors();
				// menampilkan pesan error
				print_r($error);
		} else {
				$result = $this->upload->data();

				$this->load->library('Compress');  // load the codeginiter library
				$compress = new Compress();

				$file = base_url().'images/yoshi.png'; // file that you wanna compress
				$new_name_image = $result['file_name']; // name of new file compressed
				$quality = 10; // Value that I chose
				$pngQuality = 9;
				$destination = base_url().'assets/images/resized'; // This destination must be exist on your project

				$compress->file_url = base_url()."assets/images/".$result['file_name'];
				$compress->new_name_image = $new_name_image;
				$compress->quality = $quality;
				$compress->destination = $destination;
				$compress->file_path = $result['full_path'];
				$image = $compress->compress_image();
				if($image){
					if (is_writable($result['full_path'])) {
					    unlink($result['full_path']);

							$data = [
									'title'=> $post["title"],
									'description'=> $post["deskripsi"],
									'foto'=> base_url().'assets/images/resized/'.$result['file_name']
							];
							$this->db->insert('article', $data);

							echo " <div id='alrt' style='text-align: center;
																			    margin-top: 15%;
																			    margin: auto;
																			    padding-top: 10px;
																			    width: 350px;
																			    height: 40px;
																			    border: 1px solid #00c79d;
																			    background: #edffec;
																			    border-radius: 2px;
																			    color: #636363;
																			    font-size: 15pt;'>
										</div>
									<script>
											document.getElementById('alrt').innerHTML='Data Berhasil Disimpan';
											setTimeout(function() {document.getElementById('alrt').innerHTML='';},5000);
											waitSeconds();
											function waitSeconds(iMilliSeconds) {
											    var counter= 0
											        , start = new Date().getTime()
											        , end = 0;
											    while (counter < iMilliSeconds) {
											        end = new Date().getTime();
											        counter = end - start;
											    }
											}
											document.location='".base_url()."admin/tampilArtikel';
										</script>";
					} else {
						echo "<script>
										alert('Masalah pada hak akses,file tetap ada tetapi'+
										'file aslinya tidak dapat di hapus');
										document.location='".base_url()."admin/home';
									</script>";
					}
				}
			}



	}


}
