<?php

class Admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//menampilkan halaman login pada saat pertama kali membuka web
		$this->load->view('v_home');
	}

	function login(){
		//mengambil data inputan user dari view
		$username = $this->input->post('email');
		$password = $this->input->post('password');
		//menggabungkan inputan menjadi array yang akan di proses di model
		$where = array(
			'email' => $username,
			'password' => $password
			);

		//proses pengecekan user di database
		$cek = $this->db->get_where('users',$where);
		$data = $cek->result();
		if(!empty($data)){
			//jika user di temukan maka akan di alihkan ke dashboard
			$data_session = array(
				'nama' => $username,
				'foto' => $data->foto,
				'status' => "login"
				);

			$this->session->set_userdata($data_session);

			redirect(base_url("admin/home"));

		}else{
			//jika tidak ditemukan maka akan ada notif
			echo "<script>
							alert('username atau password salah!');
							document.location='".base_url()."admin';
						</script>";
		}
	}

	function logout(){
		//menghapus session user di browser
		//note:session adalah data yang di simpan di browser
		$this->session->sess_destroy();
		redirect(base_url('admin'));
	}

	public function home()
	{
		//menampilkan halaman dashboard admin
		$this->load->view('layouts/master');
	}

}
