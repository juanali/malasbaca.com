<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$upload['upload_path']          = base_url().'assets/images/';
$upload['allowed_types']        = 'gif|jpg|png|pdf|doc';
$upload['max_size']             = 100;
$upload['max_width']            = 1024;
$upload['max_height']           = 768;
