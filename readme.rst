###################
Malas Baca
###################

Ini Adalah contoh web sederhana menggunakan framwork CodeIgniter dengan tema artikel yang mencakup AUTH,CRUD,dan UPLOAD FILE



*******************
Requirements
*******************

- Apache(server)
- Mysql(database)
- Git Bash(Tool untuk Windows)
- PHP versi 5.6 atau diatasnya lebih di sarankan


************
Installation
************

- Clone project ini di folder server anda
- Import malasbaca.sql di folder root project
- Run Website melalui browser sesuai url yang anda buat


note : Default url = http://localhost/malasbaca.com



************
Notes
************
Sumber Framework : https://www.codeigniter.com/
Sumber template tema :  https://github.com/puikinsh/gentelella
Untuk dokumentasi manual : https://www.codeigniter.com/user_guide
